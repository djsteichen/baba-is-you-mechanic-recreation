﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectScript : MonoBehaviour
{
    //access other object that collides with this object
    //check its script to see if the "playerIsWinnable" bool is true
    //if bool is true upon collision, activate win state

    public PlayerController PCScriptReference;
    public bool isWinObject;

    private void Start()
    {
        isWinObject = true;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        print("y u no work");
        if (other.CompareTag("Player"))
        {
            if (isWinObject)
            {
                print("Player collided with " + this.name);
                print("You Win!");
            }
            else
                print("Player collided with " + this.name + "and this is not a current Win Object");
        }
        
    }
}
